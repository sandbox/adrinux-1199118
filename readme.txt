Five Alive: A kit for making custom HTML5 Drupal themes
-------------------------------------------------------

The following modules should be added to any site using this theme:
http://drupal.org/project/html5_tools
http://drupal.org/project/elements

This will ensure HTML5 extenstions to core (such as to FormAPI) are also available.


I recommend not using a CSS reset, and instead using normalize.css:
http://necolas.github.com/normalize.css/

I also recommend lightweight CSS grid frameworks like Lessframework:
http://lessframework.com/


Using Five Alive
----------------

Duplicate and modify.
- clone it:
git clone
- add a branch for your own dev:
git branch
- optionally add a remote and push your custom branch to your own repo
git

