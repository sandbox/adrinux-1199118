<?php

/**
 * @file
 * Default theme implementation to display a region.
 */
?>
<footer id="<?php print $region; ?>" class="<?php print $classes; ?> clearfix">
  <?php if ($content): ?>
    <?php print $content; ?>
  <?php endif; ?>
</footer>
