<?php

/**
 * @file
 * Default theme implementation to display a region.
 */
?>
<?php if ($content): ?>
  <div id="<?php print $region; ?>" class="<?php print $classes; ?>">
    <?php print $content; ?>
  </div>
<?php endif; ?>
