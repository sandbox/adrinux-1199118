<?php

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('fivealive_rebuild_registry') && !defined('MAINTENANCE_MODE')) {
  // Rebuild .info data.
  system_rebuild_theme_data();
  // Rebuild theme registry.
  drupal_theme_rebuild();
}


/*
 *	
 *	@param $variables
 *	  A sequential array of variables to pass to the theme template.
 *	@param $hook
 *	  The name of the theme function being called ("html" in this case.)
 */
function fivealive_preprocess_html(&$variables, $hook) {
  // Always force latest IE rendering engine (even in intranet) & Chrome Frame
  if(theme_get_setting('fivealive_include_chrome_frame')){
    $element = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'http-equiv'    => 'X-UA-Compatible',
        'content' => 'IE=edge,chrome=1',
      ),
    );
  drupal_add_html_head($element,'chrome_array');
  }

  // Mobile Viewport Fix
  if (theme_get_setting('fivealive_use_mobile_viewport') && strlen(theme_get_setting('fivealive_mobile_viewport')) > 1) {
    $element = array(
      '#tag' => 'meta',
      '#attributes' => array(
        'name'    => 'viewport',
        'content' =>  theme_get_setting('fivealive_mobile_viewport'),
      ),
    );
  drupal_add_html_head($element,'mobile_viewport');
  }
}



/**
 * Provide a valid, unique HTML ID.
 */
function fivealive_preprocess_region(&$variables) {
  $variables['region'] = drupal_html_id($variables['region']);
}



/**
* Return a themed breadcrumb trail.
*
* @param $variables
* An array containing the breadcrumb links.
* @return
* A string containing the breadcrumb output.
*/
function fivealive_breadcrumb($variables) {
 $breadcrumb = $variables['breadcrumb'];

  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('fivealive_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('fivealive_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('fivealive_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('fivealive_breadcrumb_title')) {
        if ($title = drupal_get_title()) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('fivealive_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }
      return '<nav class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . "$trailing_separator$title</nav>";
    }
  }
  // Otherwise, return an empty string.
  return '';
}
